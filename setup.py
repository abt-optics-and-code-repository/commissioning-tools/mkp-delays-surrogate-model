"""
setup.py for project-start.

For reference see
https://packaging.python.org/guides/distributing-packages-using-setuptools/

"""
from pathlib import Path
from setuptools import setup, find_packages


MODULE_NAME = "mkp-delays-surrogate-model"

def read_version_from_init():
    """
    Extracts version from __init.py__. Inspired by PyJAPC.
    """
    init_file = os.path.join(
        os.path.dirname(__file__), MODULE_NAME, "__init__.py"
    )
    with open(init_file, "r") as file_handle:
        for line in file_handle:
            if line.startswith("__version__"):
                return ast.literal_eval(line.split("=", 1)[1].strip())

    raise Exception("Failed to extract version from __init__.py")


HERE = Path(__file__).parent.absolute()
with (HERE / 'README.md').open('rt', encoding='utf-8') as fh:
    LONG_DESCRIPTION = fh.read().strip()


REQUIREMENTS: dict = {
    'core': [
        "numpy",
        "matplotlib",
        "scipy",
        "gym==0.17.*",
        "pandas",
        "cernml-coi",
        "cern-general-devices",
    ],
    'test': [
        'pytest',
    ],
    'dev': [
        # 'requirement-for-development-purposes-only',
    ],
    'doc': [
        'sphinx',
        'acc-py-sphinx',
    ],
}


setup(
    name='mkp-delays-surrogate-model',
    version=read_version_from_init(),
    author='Elias Walter Waagaard',
    author_email='elias.walter.waagaard@cern.ch',
    description='Project to optimise delays of the MKP waveforms for SPS injection',
    long_description=LONG_DESCRIPTION,
    long_description_content_type='text/markdown',
    url='',
    packages=find_packages(),
    python_requires='~=3.7',
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],

    install_requires=REQUIREMENTS['core'],
    extras_require={
        **REQUIREMENTS,
        # The 'dev' extra is the union of 'test' and 'doc', with an option
        # to have explicit development dependencies listed.
        'dev': [req
                for extra in ['dev', 'test', 'doc']
                for req in REQUIREMENTS.get(extra, [])],
        # The 'all' extra is the union of all requirements.
        'all': [req for reqs in REQUIREMENTS.values() for req in reqs],
    },
    include_package_data=True,
)
