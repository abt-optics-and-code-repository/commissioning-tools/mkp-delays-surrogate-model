#MLP Boston regression example, adapted for our situation with action - loss data 

import torch
from torch import nn
from torch.utils.data import DataLoader
from sklearn.datasets import load_boston
from sklearn.preprocessing import StandardScaler
import pandas as pd
import numpy as np
import ast

use_own_data = False #use own data, or default Boston dataset, 
batch_size = 10
max_epoch_n = 20

if use_own_data == True:
    n_input = 9
    scale_data_bool = False
else:
    n_input = 13
    scale_data_bool = True
    
class BostonDataset(torch.utils.data.Dataset):
  '''
  Prepare the Boston dataset for regression
  '''

  def __init__(self, X, y, scale_data=scale_data_bool):
    if not torch.is_tensor(X) and not torch.is_tensor(y):
      # Apply scaling if necessary
      if scale_data:
          X = StandardScaler().fit_transform(X)
      self.X = torch.from_numpy(X)
      self.y = torch.from_numpy(y)

  def __len__(self):
      return len(self.X)

  def __getitem__(self, i):
      return self.X[i], self.y[i]
      

class MLP(nn.Module):
  '''
    Multilayer Perceptron for regression.
  '''
  def __init__(self):
    super().__init__()
    self.layers = nn.Sequential(
      nn.Linear(n_input, 64),
      nn.ReLU(),
      nn.Linear(64, 32),
      nn.ReLU(),
      nn.Linear(32, 1)
    )


  def forward(self, x):
    '''
      Forward pass
    '''
    return self.layers(x)

  
if __name__ == '__main__':
  
  # Set fixed random number seed
  torch.manual_seed(42)

  
  #------------ Prepare kicker dataset for pytorch, or use default Boston dataset ------------------
  if use_own_data:
      df = pd.read_csv("data_actions_rewards.csv")
      X_raw = df["actions"]
      X = np.empty((0,9), float)
      for ele in X_raw:
          #remove all double white spaces, and convert string to numpy array, then append to matrix 
          ele = " ".join(ele.split())  
          ele = ele.replace('[ ', '[')
          ele = ele.replace(' ]', ']')
          ele = ele.replace(' ', ',')
          ele = np.array(ast.literal_eval(ele))
          X = np.vstack((X, ele))
        
      y = df["out"].to_numpy()
  else:
      X, y = load_boston(return_X_y=True)
  #-----------------------------------------------------------------
  
  
  # Prepare Boston dataset
  dataset = BostonDataset(X, y)
  trainloader = torch.utils.data.DataLoader(dataset, batch_size=batch_size, shuffle=True, num_workers=1)
  
  # Initialize the MLP
  mlp = MLP()
  
  # Define the loss function and optimizer
  loss_function = nn.L1Loss() #default is L1Loss()
  optimizer = torch.optim.Adam(mlp.parameters(), lr=1e-4)
  
  """
  # Run the training loop
  for epoch in range(0, max_epoch_n): # 5 epochs at maximum
    
    # Print epoch
    print(f'Starting epoch {epoch+1}')
    
    # Set current loss value
    current_loss = 0.0
    
    # Iterate over the DataLoader for training data
    for i, data in enumerate(trainloader, 0):
      
      # Get and prepare inputs
      inputs, targets = data
      inputs, targets = inputs.float(), targets.float()
      targets = targets.reshape((targets.shape[0], 1))
      
      # Zero the gradients
      optimizer.zero_grad()
      
      # Perform forward pass
      outputs = mlp(inputs)
      
      # Compute loss
      loss = loss_function(outputs, targets)
      
      # Perform backward pass
      loss.backward()
      
      # Perform optimization
      optimizer.step()
      
      # Print statistics
      current_loss += loss.item()
      if i % 10 == 0:
          print('Loss after mini-batch %5d: %.3f' %
                (i + 1, current_loss / 500))
          current_loss = 0.0

  # Process is complete.
  print('Training process has finished.')
  #"""
  

      
  