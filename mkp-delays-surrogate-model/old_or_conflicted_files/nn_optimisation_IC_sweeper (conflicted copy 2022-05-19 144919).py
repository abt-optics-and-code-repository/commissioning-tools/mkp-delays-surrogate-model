"""
Optimisation script for trained neural network with MKP kicker delays from SPS  
--> generates many initial conditions and tries it for different optimisers 

Uses nn models whose input are normalised from [-1, 1] --> [0, 1], and whose output is normalised of the logarithm to [0, 1]
"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import torch
from torch import nn
import pickle
from scipy import optimize
import pybobyqa
from neural_network_with_validation import MLP


#Choose model to load depending on #training_epochs
n_epochs = 150  #choose number of epochs of model to load from
n_features = 9  #mkp delays: 8 individual shift, 1 general shift 
use_normalised_input = True
use_trained_only = False #use nn models which have been fully trained on the full dataset, not validated
max_iter = 1000 #maximum number of iterations for each optimiser  
saveplot = True

#Choose correct string to load model 
high_bound = 1.
if use_normalised_input :
    norm_str = '_normalised_input'
    low_bound = 0.
else: 
    norm_str = ''   
    low_bound = -1
if use_trained_only:
    train_str = '_training_only'
else:
    train_str = ''
load_str = 'mkp_delay_model{}{}'.format(norm_str, train_str)


#Initialize and load the model 
dof = 9 #all 8 individual shifts + general shift 
model = MLP(n_features)
model.load_state_dict(torch.load("nn_models/{}_{}_training_epochs.pth".format(load_str, n_epochs)))

#Create initial conditions sampled from a normal distribution centered around, then create IC vectors 
n_t = 100  #total number of IC:s to test
mean = (high_bound - np.abs(low_bound))/2
std = 0.1
Ns = [] #arrays for all normal distributions for which each MKP delay initial condition is generated (9 in total)
dtaus = []  #array for initial conditions 
for i in range(n_features):  
    N = np.random.normal(loc=mean, scale=std, size=n_t)
    Ns.append(N)
Ns = np.array(Ns)


# Define function to minimise, converting floats to tensors for the model 
# also generates plots and saves data
def minimizing_objective(dtau):
    if not torch.is_tensor(dtau):
        actions = torch.from_numpy(dtau).float()  #convert to tensor
    model.eval()
    with torch.no_grad():
        actions = torch.unsqueeze(actions, 0)  #unsqueeze to right dimensions
        loss = model(actions)  #evaluate with trained model 
    return loss.item()

#Initiate action and penalty 
actions_B = []
penalty_B = []
actions_NM = []
penalty_NM = []
actions_P = []
penalty_P = []

#Define bounds and limits for the different algorithms
upper_lim = np.ones(dof)
lower_lim = low_bound * np.ones(dof)
bounds = (lower_lim, upper_lim)
upper_lim_2 = np.ones(9)
lower_lim_2 = low_bound*np.ones(9)
bounds_2 = tuple(zip(lower_lim_2, upper_lim_2))

#Iterate over all the possible initial conditions 
for i in range(n_t):
    print("\nCommencing IC iteration {} out of {}...".format(i+1, n_t))
    
    dtau0 = Ns[:, i]
    
    # ------------ Optimize with PyBOBYQA ----------------------------------------
    #If not possible with initial conditions, add high penalty 
    try:  
        results = pybobyqa.solve(
            minimizing_objective,
            dtau0,
            bounds=bounds,
            maxfun=max_iter,
            rhobeg=0.5,
            rhoend=0.02,
            objfun_has_noise=True,
            seek_global_minimum=True,
            print_progress=False,
        )
        f_min_B = results.f
        x_min_B = results.x
        penalty_B.append(f_min_B)
        actions_B.append(x_min_B)
    except AssertionError:
        print("PyBOBYQA failed with these IC...")
        penalty_B.append(10.)
        actions_B.append(dtau0)
    #---------------------------------------------------------------------------------------"""
    
    # -------------------- Nelder-Mead and Powell Scipy ---------------------------------
  
    results_NM = optimize.minimize(minimizing_objective, dtau0, bounds=bounds_2, method='Nelder-Mead', options={'disp': False, 'maxfev' : max_iter})
    f_min_NM = results_NM.fun
    x_min_NM = results_NM.x
    penalty_NM.append(f_min_NM)
    actions_NM.append(x_min_NM)
    
    results_P = optimize.minimize(minimizing_objective, dtau0, bounds=bounds_2, method='Powell', options={'disp': False, 'maxfev' : max_iter})
    f_min_P = results_P.fun
    x_min_P = results_P.x
    penalty_P.append(f_min_P)
    actions_P.append(x_min_P)
    #---------------------------------------------------------------------------------------"""
    
    
    
    
