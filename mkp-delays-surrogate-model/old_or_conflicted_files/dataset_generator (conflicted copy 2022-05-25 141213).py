"""
Script to extract the pickled BPM positions from the runs with the MKP delay optimiser, and generate a dataset for our neural network 

Also recreates action and rewards plots for given run 
"""
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import pickle
import datetime

#Flags to save data 
reduce_high_penalty = True #reduces the high value of the penalty function to values closer to the actual BPM values 
save_dataframe = False #set False if only to test ouput data 
save_timestamp_strings = False  #only needed once, the first time
save_only_kept_beam = True #if true, then cases with lost beam of high penalty function is removed 

#Array of relevant timestamps, make to dataframe 
timestamps = [
    "2022-04-11_15-08-22.107605",  #Elias run 
    "2022-04-19_09-49-56.923005",  #Francesco run1  
    "2022-04-19_11-42-28.883047",  #Francesco run2
    "2022-04-20_10-50-05.998873"   #Francesco last run
    ]
df_ind = 0  #which one to plot
df_timestamps = pd.DataFrame(timestamps)

#Save timestamps to load elsewhere if needed 
if save_timestamp_strings:
    df_timestamps.to_csv("timestamps.csv", header=False, index=False)
    

#Unpickled the logged data 
all_data = []    
for ele in timestamps:
    #Unpickle the data 
    with open("../plots_and_data/actors_mkp_delays_log_data_{}.pickle".format(ele), "rb") as handle:
        data = pickle.load(handle)
        all_data.append(data)
        print("\nLoading data from: {}".format(ele))

#Remember that BPM positions are logged also when environment's reset method is called, so one extra value before w.r.t "actions" and rewards in "out" 
#Remove this value to make all the BPM positions correspond to a given action 
for log in all_data:
    del log["bpm_pos"][0]
    

#Convert data to dataframe 
all_df = []
for log in all_data:
    all_df.append(pd.DataFrame(log))
    
#Iterate over BPM data, remove high penalty values if desired 
all_df_fixed = []
for df in all_df:
    df = df[df['out'].values < -20.]  #remove unrealistic cases where the beam was lost but with low penalty function 
    if save_only_kept_beam:
        df = df[df['out'].values > -1e4]
    if reduce_high_penalty:
        print("\nReducing high penalty...\n")
        df['out'].values[df['out'].values < -1e4] = -1e3
    all_df_fixed.append(df)
        
#Create pandas dataframe object of all runs
df = pd.concat(all_df_fixed)
df = df.drop(columns=["bpm_pos"])  #remove BPM positions, not needed for the neural network training 
if save_dataframe:
    if save_only_kept_beam:
        df.to_csv("data_actions_rewards_only_kept_beam.csv", header=True, index=False)
    else:    
        df.to_csv("data_actions_rewards.csv", header=True, index=False)
        
#Plot the results 
fig, axis = plt.subplots(nrows=2, ncols=1, sharex=True, figsize=(16, 8))
axis[0].plot([], [])
axis[1].plot([], [])
axis[0].yaxis.label.set_size(19)
axis[0].tick_params(axis="y", labelsize=15)
axis[1].yaxis.label.set_size(19)
axis[1].xaxis.label.set_size(19)
axis[1].tick_params(axis="x", labelsize=15)
axis[1].tick_params(axis="y", labelsize=15)
legend_names = [
    "Switch 1",
    "Switch 2",
    "Switch 3",
    "Switch 4",
    "Switch 5",
    "Switch 6",
    "Switch 7",
    "Switch 8",
    "General shift",
]


#Recreate the plots for given shift
penalty = all_df_fixed[df_ind]['out']
actions_all = []
for ele in all_df_fixed[df_ind]['actions']:
    actions_all.append(ele)


#Plot the actions and loss functions 
for ax in axis:
    ax.cla()
    axis[0].plot(np.abs(penalty), color='r', marker=10, ms=8)
    axis[0].set(ylabel="Loss function $\mathcal{F}$")
    #axis[0].set_yscale("log")
    # axis[0].set_yticks([5.0, 8.0])  #add extra ticks of BPM positions
    # axis[0].get_yaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())
    axis[1].plot(actions_all, label=legend_names, marker=11, ms=8)
    axis[1].set(xlabel="#Iterations", ylabel="Actions")
    axis[1].legend(loc="right", fancybox=True, bbox_to_anchor=(1.125, 0.6), prop={'size': 16})
    plt.subplots_adjust(wspace=0, hspace=0)
    fig.canvas.draw()
    axis[0].get_shared_x_axes().join(
        axis[0], axis[1]
    )  # make the subplots share the x-axis of iterations