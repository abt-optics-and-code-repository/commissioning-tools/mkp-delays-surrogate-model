"""
Neural network builder, inspired by example from R. RAMJIAWAN
"""
import numpy as np
import pandas as pd
import pickle
import matplotlib.pyplot as plt
import seaborn as sns
import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import Dataset, DataLoader
from torch.utils.data.sampler import SubsetRandomSampler
from tqdm.notebook import tqdm
from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import MaxAbsScaler  #to normalize output data 
from sklearn.model_selection import train_test_split
from sklearn.datasets import load_boston

#Initializing own data
use_own_data = False #use own data (dim(X) = 923x9), or Boston dataset from scikit learn (dim(X) = 506x13)
EPOCHS = 1500
BATCH_SIZE = 64
LEARNING_RATE = 0.15
torch.manual_seed(1)    # so is reproducible

#REBECCA'S previous data input 
#X_init = np.hstack((x_bpm[:,:-5]*10**6, x_prediction[:,:]*10**6, y_bpm[:,:-5]*10**6, y_prediction[:,:]*10**6))   # in um, 5 measured offsets and 10 predicted offsets
#y_init = np.hstack(((x_bpm[:,-5:] - x_prediction[:,-5:])*10**6, (y_bpm[:,-5:] - y_prediction[:,-5:])*10**6)) # in um

#Load the data
if use_own_data:
    a = pickle.load(open('data_actions_rewards', 'rb'))
    X_init = a['X']
    y_init = a['y']
else:
    X_init, y_init = load_boston(return_X_y=True)

# Train - Test split using scikit-learn
X_trainval, X_test, y_trainval, y_test = train_test_split(X_init, y_init, test_size=0.2, random_state=42)# Split train into train-val
X_train, X_val, y_train, y_val = train_test_split(X_trainval, y_trainval, test_size=0.1, random_state=21)

X_train = (X_train -np.mean(X_train,axis=0)) # calculates and transforms
X_test = (X_test -np.mean(X_train,axis=0)) # transforms
X_val = (X_val -np.mean(X_train,axis=0)) # transforms
y_train = (y_train -np.mean(y_train,axis=0)) # calculates and transforms
y_test = (y_test -np.mean(y_train,axis=0)) # transforms
y_val = (y_val -np.mean(y_train,axis=0)) # transforms

# Normalise 
a1 = np.round(np.min(X_train, axis=0),-1)
a2 = np.round(np.max(X_train, axis = 0),-1)
norm_X = np.maximum(abs(a1), abs(a2))
a1 = np.round(np.min(y_train, axis=0),-1)
a2 = np.round(np.max(y_train, axis = 0),-1)
norm_y = np.maximum(abs(a1), abs(a2))

X_train = (X_train / norm_X[None,:]) 
X_test = (X_test / norm_X[None,:])
X_val = (X_val / norm_X[None,:])
y_train = (y_train / norm_y) 
y_test = (y_test / norm_y)
y_val = (y_val / norm_y)

"""#uncomment if want to save a pickled file
norm = {}
norm['X'] = norm_X
norm['y'] = norm_y
filename = 'norm'
outfile = open(filename,'wb')
pickle.dump(norm,outfile)
outfile.close()
#"""

# Convert the output variable to float
y_train, y_test, y_val = y_train.astype(float), y_test.astype(float), y_val.astype(float)

class RegressionDataset(Dataset):
    def __init__(self, X_data, y_data):
        self.X_data = X_data
        self.y_data = y_data
        
    def __getitem__(self, index):
        return self.X_data[index], self.y_data[index]
        
    def __len__ (self):
        return len(self.X_data)
    
train_dataset = RegressionDataset(torch.from_numpy(X_train).float(), torch.from_numpy(y_train).float())
val_dataset = RegressionDataset(torch.from_numpy(X_val).float(), torch.from_numpy(y_val).float())
test_dataset = RegressionDataset(torch.from_numpy(X_test).float(), torch.from_numpy(y_test).float())

NUM_FEATURES = (np.shape(X_init)[1])
print("number of features = " +str(NUM_FEATURES))

# # Initialise dataloader
train_loader = DataLoader(dataset=train_dataset, batch_size=BATCH_SIZE, shuffle=True)
test_loader = DataLoader(dataset=test_dataset, batch_size=1)
val_loader = DataLoader(dataset=val_dataset, batch_size=1)


# define NN architecture

class MultipleRegression(nn.Module):
    def __init__(self, num_features):
        super().__init__()
        
        self.layer_1 = nn.Linear(num_features, 40)   # too small
        self.layer_2 = nn.Linear(40, 50)
        self.layer_3 = nn.Linear(50, 60)
        self.layer_4 = nn.Linear(60, 50)
        self.layer_5 = nn.Linear(50, 40)
        self.layer_6 = nn.Linear(40, 20)
        self.layer_out = nn.Linear(20, 10)
        
        self.tanh = nn.Tanh()
    def forward(self, inputs):
        x = self.tanh(self.layer_1(inputs))
        x = self.tanh(self.layer_2(x))
        x = self.tanh(self.layer_3(x))
        x = self.tanh(self.layer_4(x))
        x = self.tanh(self.layer_5(x))
        x = self.tanh(self.layer_6(x))
        x = self.layer_out(x)
        return x


device = "cpu"
model = MultipleRegression(NUM_FEATURES)
model.to(device)

criterion = nn.MSELoss() ###sum
optimizer = optim.SGD(model.parameters(), lr=LEARNING_RATE, weight_decay=1e-5)

loss_stats = {
    'train': [],
    "val": []
}

print("Begin training.")
for e in tqdm(range(1, EPOCHS+1)):
    # TRAINING
    if e == 100:
        for g in optimizer.param_groups:
            g['lr'] = 0.13
    if e == 200:
        for g in optimizer.param_groups:
            g['lr'] = 0.10
    if e == 500:
        for g in optimizer.param_groups:
            g['lr'] = 0.05
    if e == 700:
        for g in optimizer.param_groups:
            g['lr'] = 0.02
    train_epoch_loss = 0
    model.train()
    for X_train_batch, y_train_batch in train_loader:
        X_train_batch, y_train_batch = np.squeeze(X_train_batch.to(device)), y_train_batch.to(device)
        optimizer.zero_grad()
        
        y_train_pred = model(X_train_batch)
        train_loss = criterion(np.squeeze(y_train_pred), np.squeeze(y_train_batch))
        
        train_loss.backward()
        optimizer.step()
        
        train_epoch_loss += train_loss.item()
        
        
    # VALIDATION    
    with torch.no_grad():
        
        val_epoch_loss = 0
        
        model.eval()
        for X_val_batch, y_val_batch in val_loader:
            X_val_batch, y_val_batch = X_val_batch.to(device), y_val_batch.to(device)
            
            y_val_pred = model(X_val_batch)
                        
            val_loss = criterion(np.squeeze(y_val_pred), np.squeeze(y_val_batch))
            
            val_epoch_loss += val_loss.item()
        loss_stats['train'].append(train_epoch_loss/len(train_loader))
        loss_stats['val'].append(val_epoch_loss/len(val_loader))                              
    
#         print(f'Epoch {e+0:03}: | Train Loss: {train_epoch_loss/len(train_loader):.5f} | Val Loss: {val_epoch_loss/len(val_loader):.5f}')

train_val_loss_df = pd.DataFrame.from_dict(loss_stats).reset_index().melt(id_vars=['index']).rename(columns={"index":"epochs"})

plt.figure(figsize=(10,7))
sns.lineplot(data=train_val_loss_df, x = "epochs", y="value", hue="variable")   
plt.yscale('log')
plt.show()
