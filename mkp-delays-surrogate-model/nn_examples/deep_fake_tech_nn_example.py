"""
Example neural network with Deep Fake Tech from F. Velotti
"""

import numpy as np
import matplotlib.pyplot as plt
from case.data.load_data import CaseDataset, ToTensor
from torch.utils.data import DataLoader, random_split
import tqdm
import torch
from torch import nn
from torch import optim
import os
from pathlib import Path
import pickle

MODEL_NAME = "deep_fake_tecs"

PATH_STORAGE = Path(__file__).parent.absolute() / "trained_models"

try:
    os.mkdir(PATH_STORAGE / MODEL_NAME)
except FileExistsError:
    print("++++ Overwriting files - folder exists already!")

data = CaseDataset(
    save_loc=PATH_STORAGE / MODEL_NAME, transform=ToTensor()
)

train_data, test_data = random_split(
    data, [int(len(data) * 0.7), int(len(data) * 0.3)]
)


class MLPcase(nn.Module):
    def __init__(
        self,
        input_shape: int = 3,
        hidden_size: int = 256,
        device="cuda",
    ):
        super().__init__()
        self.hidden_size = hidden_size
        self.dense = nn.Linear(input_shape, self.hidden_size).to(device)
        self.dense_2 = nn.Linear(self.hidden_size, self.hidden_size).to(
            device
        )
        self.rlu = nn.Tanh().to(device)
        self.last = nn.Linear(self.hidden_size, 1).to(device)

    def forward(self, x):
        out = self.dense(x)
        out = self.rlu(out)
        out = self.dense_2(out)
        out = self.rlu(out)
        out = self.last(out)

        return out


def init_weights(m):
    for name, param in m.named_parameters():
        nn.init.normal_(param.data, mean=0, std=0.01)


def count_parameters(model):
    return sum(p.numel() for p in model.parameters() if p.requires_grad)


device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
print(device)


def loss_batch(model, x, y, loss_function, opt=None):

    output = model(x)
    # y = y.squeeze(-1)
    loss = loss_function(output, y)

    if opt is not None:
        loss.backward()
        opt.step()
        opt.zero_grad()

    return loss.item(), len(y)


def fit(epochs, model, optimiser, loss_function, train_dl, val_dl):

    train_loss = np.zeros(epochs)
    val_loss = np.zeros(epochs)

    for epoch in range(epochs):

        model.train()

        progress = tqdm.tqdm(
            train_dl, total=len(train_dl), ascii=[" ", "="], ncols=100
        )

        loss_epoch = []
        samples_epoch = []

        for x_train, y_train in progress:
            x_train, y_train = (
                x_train.to(device),
                y_train.to(device),
            )

            loss, items = loss_batch(
                model,
                x_train,
                y_train,
                loss_function,
                opt=optimiser,
            )

            loss_epoch.append(loss)
            samples_epoch.append(items)

            progress.set_description(f"Epoch [{epoch + 1} / {epochs}]")
            val_loss_visual = val_loss[epoch - 1] if epoch > 1 else 0
            progress.set_postfix(loss=loss, val_loss=val_loss_visual)

        train_loss[epoch] = np.sum(
            np.multiply(loss_epoch, samples_epoch)
        ) / np.sum(samples_epoch)

        model.eval()
        loss_val = []
        items_val = []
        with torch.no_grad():
            for x_v, y_v in val_dl:
                loss, items = loss_batch(
                    model,
                    x_v.to(device),
                    y_v.to(device),
                    loss_function,
                )
                loss_val.append(loss)
                items_val.append(items)

        val_loss_epoch = np.sum(
            np.multiply(loss_val, items_val)
        ) / np.sum(items_val)
        val_loss[epoch] = val_loss_epoch

    history = {"loss": train_loss, "val_loss": val_loss}
    return history


BATCH_SIZE = 64
LR = 1e-3

EPOCHS = 200

print(len(train_data), len(test_data))
train_dl = DataLoader(train_data, batch_size=BATCH_SIZE, shuffle=True)
test_dl = DataLoader(test_data, batch_size=BATCH_SIZE, shuffle=True)

model = MLPcase(device=device)
model.apply(init_weights)

optimiser = optim.Adam(model.parameters(), lr=LR)
# optimiser = optim.SGD(model.parameters(), lr=0.01, momentum=0.9)

criterion = nn.MSELoss(reduction="sum")
print(count_parameters(model))


if "model.pt" not in os.listdir(PATH_STORAGE / MODEL_NAME):
    hisotry_fit = fit(
        EPOCHS, model, optimiser, criterion, train_dl, test_dl
    )

    pickle.dump(
        hisotry_fit,
        open(PATH_STORAGE / MODEL_NAME / "training_hist.p", "wb"),
    )

    torch.save(
        model.state_dict(), PATH_STORAGE / MODEL_NAME / "model.pt"
    )

else:
    hisotry_fit = pickle.load(
        open(PATH_STORAGE / MODEL_NAME / "training_hist.p", "rb")
    )
    model.load_state_dict(
        torch.load(PATH_STORAGE / MODEL_NAME / "model.pt", map_location=torch.device('cpu'))
    )

plt.figure()
plt.plot(hisotry_fit["loss"])
plt.plot(hisotry_fit["val_loss"])
plt.show()

print(test_data[:][0].shape)

model.eval()
y_pred_test = model(test_data[:][0].to(device)).cpu().detach().numpy()
y_pred_train = model(train_data[:][0].to(device)).cpu().detach().numpy()


plt.figure()
plt.subplot(211)
plt.plot(
    train_data[:][1].squeeze(), y_pred_train.squeeze(), ".", label="NN"
)
plt.plot(
    train_data[:][1].squeeze(),
    train_data[:][1].squeeze(),
    label="Ideal",
)
plt.xlabel("Ground truth (training)")
plt.ylabel("Prediction")
plt.legend()
plt.subplot(212)
plt.plot(
    test_data[:][1].squeeze(), y_pred_test.squeeze(), ".", label="NN"
)
plt.plot(
    test_data[:][1].squeeze(), test_data[:][1].squeeze(), label="Ideal"
)
plt.xlabel("Ground truth (test)")
plt.ylabel("Prediction")
plt.legend()
plt.show()

# Test loss map reproduction

losses = data.data_all[41.0]
angle = data.angle
pos = data.pos

plt.figure(figsize=(6, 2))
plt.subplot(121)
plt.title("Original")
levels = np.linspace(np.min(losses), np.max(losses), 15)
levels_small = np.linspace(0.2, 1.6, 5)
X, Y = np.meshgrid(angle * 1e6, pos * 1e3)
CS1 = plt.contour(
    Y,
    X,
    losses.T,
    levels=[0.0, 0.27],
    ls="-",
    colors="r",
    linewidths=0.5,
)
CS = plt.contour(
    Y,
    X,
    losses.T,
    levels=[0.5, 0.6, 1.0, 1.3],
    ls="-",
    colors="w",
    linewidths=0.5,
)
plt.contourf(Y, X, losses.T, levels=levels, cmap="magma", alpha=1)
plt.colorbar(label="Relative loss")
plt.clabel(CS, inline=1, fontsize=6, colors="w")

plt.xlabel("position / mm")

plt.ylabel(r"angle / $\mu$rad")


# Deep fake losses

angle_dfl = data.angle
pos_dfl = data.pos

losses_dfl = np.zeros((len(angle_dfl), len(pos_dfl)))

for i, ang in enumerate(angle_dfl):
    for j, pos in enumerate(pos_dfl):
        model.eval()
        x = np.array([ang, pos, 41.0, 0.0]).reshape(1, -1)
        x = data.normalise(x)
        x = x[:, :-1]
        loss_pred = (
            model(torch.Tensor(x).to(device)).cpu().detach().numpy()
        )
        loss_pred = np.array([0, 0, 0, loss_pred]).reshape(1, -1)
        losses_dfl[i, j] = data.unormalise(loss_pred)[:, -1]


plt.subplot(122)
plt.title("Deep fake")
levels = np.linspace(np.min(losses_dfl), np.max(losses_dfl), 15)
levels_small = np.linspace(0.2, 1.6, 5)
X, Y = np.meshgrid(angle_dfl * 1e6, pos_dfl * 1e3)
CS1 = plt.contour(
    Y,
    X,
    losses_dfl.T,
    levels=[0.0, 0.27],
    ls="-",
    colors="r",
    linewidths=0.5,
)
CS = plt.contour(
    Y,
    X,
    losses_dfl.T,
    levels=[0.5, 0.6, 1.0, 1.3],
    ls="-",
    colors="w",
    linewidths=0.5,
)
plt.contourf(Y, X, losses_dfl.T, levels=levels, cmap="magma", alpha=1)
plt.colorbar(label="Relative loss")
plt.clabel(CS, inline=1, fontsize=6, colors="w")

plt.xlabel("position / mm")

plt.ylabel(r"angle / $\mu$rad")

plt.show()
